﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofertapp.Modelo
{
    internal class Oferta
    {
        public Image Imagen {  get; set; }
        public double Precio {  get; set; }
        public string Descripcion { get; set; }
        public DateTime VigenciaDesde { get; set; }
        public DateTime VigenciaHasta { get; set; }

        public Oferta(Image imagen, double precio, string descripcion, DateTime vigenciaDesde, DateTime vigenciaHasta)
        {
            Imagen = imagen;
            Precio = precio;
            Descripcion = descripcion; 
        }
    
    }
}
