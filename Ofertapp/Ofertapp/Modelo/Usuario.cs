﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Ofertapp.Modelo
{
    internal class Usuario
    {
        public string Name { get; set; }
        public MailAddress Address { get; set; }

        public Usuario(string name, MailAddress address) { 
            Name = name;
            Address = address;
        }
    }
}
