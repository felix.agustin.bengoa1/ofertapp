﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ofertapp.Modelo
{
    internal class Publicacion
    {
        public Usuario Usuario;
        public Oferta Oferta;
        public double Votacion {  get; set; }
        public Publicacion(Usuario usuario, Oferta oferta, double votacion) {
            Usuario = usuario;
            Oferta = oferta;
            Votacion = votacion;
        }
    }
}
