﻿namespace Ofertapp
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            LblFoto = new Label();
            LblDescripcion = new Label();
            LblVigenciaDesde = new Label();
            LblVigenciaHasta = new Label();
            BtnPublicar = new Button();
            LblPrecio = new Label();
            TxtPrecio = new TextBox();
            TxtDescripcion = new TextBox();
            ImgOferta = new PictureBox();
            DttVigenciaDesde = new DateTimePicker();
            DttVigenciaHasta = new DateTimePicker();
            CmbOferta = new ComboBox();
            ((System.ComponentModel.ISupportInitialize)ImgOferta).BeginInit();
            SuspendLayout();
            // 
            // LblFoto
            // 
            LblFoto.AutoSize = true;
            LblFoto.Location = new Point(79, 46);
            LblFoto.Name = "LblFoto";
            LblFoto.Size = new Size(39, 20);
            LblFoto.TabIndex = 0;
            LblFoto.Text = "Foto";
            // 
            // LblDescripcion
            // 
            LblDescripcion.AutoSize = true;
            LblDescripcion.Location = new Point(79, 152);
            LblDescripcion.Name = "LblDescripcion";
            LblDescripcion.Size = new Size(87, 20);
            LblDescripcion.TabIndex = 1;
            LblDescripcion.Text = "Descripcion";
            // 
            // LblVigenciaDesde
            // 
            LblVigenciaDesde.AutoSize = true;
            LblVigenciaDesde.Location = new Point(79, 195);
            LblVigenciaDesde.Name = "LblVigenciaDesde";
            LblVigenciaDesde.Size = new Size(118, 20);
            LblVigenciaDesde.TabIndex = 2;
            LblVigenciaDesde.Text = "vigencia (desde)";
            // 
            // LblVigenciaHasta
            // 
            LblVigenciaHasta.AutoSize = true;
            LblVigenciaHasta.Location = new Point(79, 231);
            LblVigenciaHasta.Name = "LblVigenciaHasta";
            LblVigenciaHasta.Size = new Size(113, 20);
            LblVigenciaHasta.TabIndex = 3;
            LblVigenciaHasta.Text = "vigencia (hasta)";
            // 
            // BtnPublicar
            // 
            BtnPublicar.Location = new Point(79, 288);
            BtnPublicar.Name = "BtnPublicar";
            BtnPublicar.Size = new Size(118, 29);
            BtnPublicar.TabIndex = 5;
            BtnPublicar.Text = "Publicar Oferta";
            BtnPublicar.UseVisualStyleBackColor = true;
            BtnPublicar.Click += button1_Click;
            // 
            // LblPrecio
            // 
            LblPrecio.AutoSize = true;
            LblPrecio.Location = new Point(79, 119);
            LblPrecio.Name = "LblPrecio";
            LblPrecio.Size = new Size(50, 20);
            LblPrecio.TabIndex = 6;
            LblPrecio.Text = "Precio";
            // 
            // TxtPrecio
            // 
            TxtPrecio.Location = new Point(208, 112);
            TxtPrecio.Name = "TxtPrecio";
            TxtPrecio.Size = new Size(125, 27);
            TxtPrecio.TabIndex = 7;
            // 
            // TxtDescripcion
            // 
            TxtDescripcion.Location = new Point(208, 145);
            TxtDescripcion.Name = "TxtDescripcion";
            TxtDescripcion.Size = new Size(125, 27);
            TxtDescripcion.TabIndex = 8;
            // 
            // ImgOferta
            // 
            ImgOferta.Location = new Point(208, 46);
            ImgOferta.Name = "ImgOferta";
            ImgOferta.Size = new Size(125, 62);
            ImgOferta.TabIndex = 9;
            ImgOferta.TabStop = false;
            // 
            // DttVigenciaDesde
            // 
            DttVigenciaDesde.Location = new Point(208, 195);
            DttVigenciaDesde.Name = "DttVigenciaDesde";
            DttVigenciaDesde.Size = new Size(250, 27);
            DttVigenciaDesde.TabIndex = 10;
            // 
            // DttVigenciaHasta
            // 
            DttVigenciaHasta.Location = new Point(208, 231);
            DttVigenciaHasta.Name = "DttVigenciaHasta";
            DttVigenciaHasta.Size = new Size(250, 27);
            DttVigenciaHasta.TabIndex = 11;
            // 
            // CmbOferta
            // 
            CmbOferta.FormattingEnabled = true;
            CmbOferta.Location = new Point(492, 41);
            CmbOferta.Name = "CmbOferta";
            CmbOferta.Size = new Size(283, 28);
            CmbOferta.TabIndex = 12;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(CmbOferta);
            Controls.Add(DttVigenciaHasta);
            Controls.Add(DttVigenciaDesde);
            Controls.Add(ImgOferta);
            Controls.Add(TxtDescripcion);
            Controls.Add(TxtPrecio);
            Controls.Add(LblPrecio);
            Controls.Add(BtnPublicar);
            Controls.Add(LblVigenciaHasta);
            Controls.Add(LblVigenciaDesde);
            Controls.Add(LblDescripcion);
            Controls.Add(LblFoto);
            Name = "Form1";
            Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)ImgOferta).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label LblFoto;
        private Label LblDescripcion;
        private Label LblVigenciaDesde;
        private Label LblVigenciaHasta;
        private Button BtnPublicar;
        private Label LblPrecio;
        private TextBox TxtPrecio;
        private TextBox TxtDescripcion;
        private PictureBox ImgOferta;
        private DateTimePicker DttVigenciaDesde;
        private DateTimePicker DttVigenciaHasta;
        private ComboBox CmbOferta;
    }
}
